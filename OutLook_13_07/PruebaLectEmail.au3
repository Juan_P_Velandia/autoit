#include <OutlookEX.au3>
Global $aAttachments

; Connect to Outlook
Global $oOutlook = _OL_Open()
If @error <> 0 Then Exit MsgBox(16, "OutlookEX UDF", "Error opening connection to Outlook. @error = " & @error & ", @extended = " & @extended)

; Access the Inbox
Global $aFolder = _OL_FolderAccess($oOutlook, "", $olFolderInbox)
If @error <> 0 Then Exit MsgBox(16, "OutlookEX UDF", "Error accessing the Inbox. @error = " & @error & ", @extended = " & @extended)

;Leer arbol de carpetas
Global $iLevel = 9999
Global $nTreeF =_OL_FolderTree($oOutlook, $aFolder[1],$iLevel)
Global $nItems = _OL_ItemFind($oOutlook, $aFolder[1], $olMail, "[UnRead]=True", "", "", "EntryID,Subject,SenderEmailAddress", "", 4)

;busqueda por asunto
;Global $busqueda = "Fwd: Emision de comprobante virtual"
;Global $aSearchArray[2][4] = [[1, 4],[0x0037001E, 1, $busqueda]]

; Search for unread items with attachments
;Global $sFilter = "@SQL=""urn:schemas:httpmail:hasattachment"" = 1 and ""urn:schemas:httpmail:read""=0"
Global $sFilter = "@SQL=""urn:schemas:httpmail:read""=0"
Global $aResult = _OL_ItemSearch($oOutlook, $aFolder[1], $sFilter, "EntryID,subject,SenderEmailAddress")
;Global $aResult = _OL_ItemSearch($oOutlook, $aFolder[1], $aSearchArray, "EntryID,subject,urn:schemas:httpmail:textdescription,SenderEmailAddress")  ;linea busqueda por asunto
If @error Then
    MsgBox(16, "OutlookEX UDF - _OL_ItemSearch Example Script", "Error running _OL_ItemSearch. @error = " & @error & ", @extended = " & @extended)
Else

	_ArrayDisplay($nTreeF, "Outlook Informacion de arbol desde el folder" & $nItems)
    _Arraydisplay($aResult, "Example 6")
EndIf



; Now get a list of attachments
For $i = 1 to (UBound($aResult)-1)
	$oItem = $oOutlook.Session.GetItemFromID($aResult[$i][0], Default) ; con el Id de Array $aResult , se optiene el Item
	$oItem.GetInspector
	$sBody = $oItem.Body ; cuerpo del mensaje
	;$oItem.Unread = False ; Set the item to read  (funcional)


	; Display the information and contents of the email
	ConsoleWrite("Este es el correo: "& $i & @LF)
	ConsoleWrite("  " & $sBody & @CRLF) ; impresion por consola del Cuerpo
	;MsgBox(0, "OutlookEX UDF - Cuerpo del correo", $sBody ) ; mesangge box mal configurado para ver el mensaje
	$Dir = "C:\Users\jvela\Desktop\OutLook_13_07/"  ;definicion ruta almacenamiento de archivo
	$archivo = FileOpen($Dir & ("BodyMail" & $i & ".txt"), 2)  ;creacion de archivo
	FileWrite($archivo, $sBody  & @CRLF)  ; escritura del cuerpo
	FileClose($archivo)  ;cerrar archivo
Next


_OL_Close($oOutlook)
Exit