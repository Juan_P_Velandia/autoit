#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
MouseMove(100,120) ; Coordenadas x,y
MouseMove(200,480,30) ; El tercer número, el 30, es la velocidad del ratón.
MouseMove(500,650,20)
MouseMove(300,650,70)
MouseMove(500,650,10)
$posicion = MouseGetPos()
MsgBox(0, "Posición:", $posicion[0] & ", " & $posicion[1])

Sleep(3000)

MouseClick("right"); Pulsa con el botón derecho del ratón.
