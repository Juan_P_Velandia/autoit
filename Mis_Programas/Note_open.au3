#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
Example()

Func Example()
    ; Run Notepad with the window maximized.
    Local $iPID = Run("notepad.exe", "", @SW_SHOWMAXIMIZED)

    ; Wait 10 seconds for the Notepad window to appear.
    WinWait("[CLASS:Notepad]", "", 10)

    ; Wait for 2 seconds.

	Send("{SHIFTDOWN}ad{SHIFTUP}01{TAB}{SHIFTDOWN}geteco{SHIFTUP}20")
	Sleep(5000)
    ; Close the Notepad process using the PID returned by Run.
    ProcessClose($iPID)
EndFunc   ;==>Example
