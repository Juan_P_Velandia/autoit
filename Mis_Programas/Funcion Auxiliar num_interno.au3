#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------
Local $num = find_num("0010026112111")
ConsoleWrite($num)
; Script Start - Add your code below here
Func find_num($num_interno) ;Corrige el numero interno si viene con menos numeros
	Local $var_1 = StringLen($num_interno)
	ConsoleWrite($var_1 & @CRLF)
	Local $var_2 = StringLeft($num_interno, 6)
	ConsoleWrite($var_2 & @CRLF)
	Local $var_3 = StringLen($var_2)
	ConsoleWrite($var_3 & @CRLF)
	Local $var_4 = $var_1 - $var_3
	ConsoleWrite($var_4 & @CRLF)
	Local $var_5 = StringRight($num_interno, $var_4)
	ConsoleWrite($var_5 & @CRLF)
	If $var_4 = "8" Then
		ConsoleWrite("OK numero interno")
		Return $num_interno
	ElseIf $var_4 <= "8" Then
		Local $var_7 = "0"
		For $i = $var_4 + 1 To 7 Step 1
			$var_7 = $var_7 & "0"
		Next
		ConsoleWrite($var_7 & @CRLF)
		$num_interno = $var_2 & $var_7 & $var_5
		ConsoleWrite($num_interno&@CRLF)
		Return $num_interno
	Else
	ConsoleWrite("numero interno" &$num_interno &"mal")
	EndIf
EndFunc   ;==>find_num
