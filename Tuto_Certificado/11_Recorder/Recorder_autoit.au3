#region --- Au3Recorder generated code Start (v3.3.9.5 KeyboardLayout=0000080A)  ---

#region --- Internal functions Au3Recorder Start ---
Func _Au3RecordSetup()
Opt('WinWaitDelay',100)
Opt('WinDetectHiddenText',1)
Opt('MouseCoordMode',0)
Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
If $aResult[1] <> '0000080A' Then
  MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(0000080A->' & $aResult[1] & ')')
EndIf

EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

_AU3RecordSetup()
#endregion --- Internal functions Au3Recorder End ---

_WinWaitActivate("Liste Annuaire","Nom prénom")
MouseClick("left",45,41,1)
MouseClick("left",126,155,1)
Send("{TAB}{TAB}")
MouseClick("left",132,230,1)
MouseClick("right",109,149,1)
MouseClick("left",50,48,1)
#endregion --- Au3Recorder generated code End ---
