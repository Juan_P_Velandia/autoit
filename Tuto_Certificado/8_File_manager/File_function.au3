#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

;~ Function that gathers a certain line
Func Readfromfile($path, $number)
;~ 	always start with file open
	Local $handle = FileOpen($path, 0)
	Local $line = FileReadLine($handle, $number)
	MsgBox(0, "Line Read", $line)
EndFunc   ;==>Readfromfile

;~ Readfromfile("My_text_file.txt", 1)
;~ Readfromfile("My_text_file.txt", 2)
;~ Readfromfile("My_text_file.txt", 3)
;~ Readfromfile("My_text_file.txt", 4)

;~ Funcion que me permite escribir nuevo contenido en un archivo
Func writeFunction($path,$mystring)
;~ 	the mode ride is 2 in fileopen
	Local $handle = FileOpen($path,2)
	FileWrite($handle,$mystring)
;~ 	Vaciar el buffer del disco
	FileFlush($handle)
	FileClose($handle)
MsgBox(0,"end","Finish to write "&$path)
EndFunc   ;==>writeFunction

;~ writeFunction("Myfile.txt","Hola Bebesita")

Func AppendFunction($path,$mystring)
	Local  $handle = FileOpen($path,1)
	FileWrite($handle,$mystring)
;~ 	Vaciar el buffer del disco
	FileFlush($handle)
	FileClose($handle)
	EndFunc
 AppendFunction("Myfile.txt","How are you? ")
 AppendFunction("Myfile.txt","I am Good ")

