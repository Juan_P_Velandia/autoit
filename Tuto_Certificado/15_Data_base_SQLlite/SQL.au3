#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.10.2
 Author:         myName

 Script Function:
	Template AutoIt script.
#ce ----------------------------------------------------------------------------


; Script Start - Add your code below here

#include <SQLite.au3>
#include <SQLite.dll.au3>

local $myarray,$line,$path="contacts.csv"
Local $handle = FileOpen($path,0)


_SQLite_Startup ()
$MyDB=_SQLite_Open ("MyDB1")
_SQLite_Exec (-1, "CREATE TABLE persons (Name, Age,Adress,Email);")

While 1
	$line=FileReadLine($handle)
	if @error then ExitLoop
	$myarray=StringSplit($line,",")
	_SQLite_Exec (-1, "INSERT INTO persons VALUES ('"&$myarray[1]&"','"&$myarray[2]&"','"&$myarray[3]&"','"&$myarray[4]&"');")
WEnd
_SQLite_Close ()
_SQLite_Shutdown ()
