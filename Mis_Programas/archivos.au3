;Crea un directorio en el disco C
; llamado la carpeta de autoit
DirCreate("C:/Carpeta de AutoIt/")
DirCreate(@DesktopDir & "/La carpeta de Autoit")
;~ Para borrar se utilizan los siguientes comandos :
;~  DirRemove("C:/Carpeta de AutoIt/", 1)
;~  DirRemove (@DesktopDir & "/La carpeta de Autoit", 1)
;~ SI tiene un 1 borrara lo que contenga adentro
;~ Si ponemos 0 borrara si est� basio y no tiene nada
;~ Para mover Directorio completo se utiliza mediante
;~ DirMove("C:\Esta carpeta\", "C:\Otro sitio\Aqu� se mover�", 1 )

;~ Para crear un archivo Vacio
$carpeta = "C:/Carpeta de AutoIt/"
DirCreate($carpeta)
$archivo = FileOpen($carpeta & "prueba.txt", 2)
;~ Datos para tener en cuenta
;~    0 = Modo de lectura
;~    1 = Modo de escritura (adjunta al final de la l�nea del archivo)
;~    2 = Modo escritura 2 (borra el contenido previo del archivo)
;~    Modos 1 y 2 (de escritura) pueden ser creados si el archivo no existe.
;~    @CRLF es cambio de linea en el text file
FileWrite($archivo, "Esta es la L�nea 1" & @CRLF)
FileWrite($archivo, "Esta es la L�nea 2" & @CRLF)
FileWrite($archivo, "Esta es la Linea 3" & @CRLF)
FileWriteLine($archivo, "Otra Linea 4")
FileWriteLine($archivo, "Otra Linea 5")

FileClose($archivo)
;~ Para leer parte de archivo
$archivo = FileOpen($carpeta & "prueba.txt", 0)
$texto = FileRead($archivo, 23)
MsgBox(0, "Lectura", $texto)
FileClose($archivo)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;~ Leer archivo completo
$archivo = FileOpen($carpeta & "prueba.txt", 0)
$Completo = ""
While 1
	$letra = FileRead($archivo, 1)
	$Completo = $Completo & $letra
	if @error = -1 Then ExitLoop
WEnd
MsgBox(0, "El archivo completo con FileRead bucle", $Completo)
FileClose($archivo)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;