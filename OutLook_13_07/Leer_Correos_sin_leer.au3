;#AutoIt3Wrapper_AU3Check_Parameters= -d -w 1 -w 2 -w 3 -w 4 -w 5 -w 6
;#AutoIt3Wrapper_AU3Check_Stop_OnWarning=N
#include <OutlookEX.au3>
Global $aAttachments

; Conexion a Outlook
Global $oOutlook = _OL_Open()
If @error <> 0 Then Exit MsgBox(16, "OutlookEX UDF", "Error opening connection to Outlook. @error = " & @error & ", @extended = " & @extended)

; acceso a folder Inbox
Global $aFolder = _OL_FolderAccess($oOutlook, "", $olFolderInbox)
If @error <> 0 Then Exit MsgBox(16, "OutlookEX UDF", "Error accessing the Inbox. @error = " & @error & ", @extended = " & @extended)



; Busqueda por Asunto , para revisar el comprobante enviado por mingo
;Global $sFilter = "@SQL=""urn:schemas:httpmail:hasattachment"" = 1 and ""urn:schemas:httpmail:read""=0"

Global $busqueda = "Revision 2" ; busqueda de asunto
Global $aSearchArray[2][4] = [[1, 4],[0x0037001E, 1, $busqueda]] ;array de filtro o busqueda

Global $sFilter = "@SQL=""urn:schemas:httpmail:read""=0" ; fitro mensajes no leidos , no usado por ahora
Global $aResult = _OL_ItemSearch($oOutlook, $aFolder[1], $aSearchArray, "EntryID,subject,urn:schemas:httpmail:textdescription,SenderEmailAddress");funcion de busqueda de item o correo segun array de busqueda
If @error Then
    MsgBox(16, "OutlookEX UDF - _OL_ItemSearch Example Script", "Error running _OL_ItemSearch. @error = " & @error & ", @extended = " & @extended)
Else
	_Arraydisplay($aResult, "Resultado de array de busqueda //	 _OL_ItemSearch() ")
EndIf


$oItem = $oOutlook.Session.GetItemFromID($aResult[1][0], Default) ; con el Id de Array $aResult , se optiene el Item
$oItem.GetInspector
$sBody = $oItem.Body ; cuerpo del mensaje

$oItem.Unread = False ; Set the item to read


; Display the information and contents of the email
ConsoleWrite("  " & $sBody & @CRLF) ; impresion por consola del Cuerpo
;MsgBox(0, "OutlookEX UDF - Cuerpo del correo", $sBody ) ; mesangge box mal configurado para ver el mensaje





; Now get a list of attachments of this email only first
For $i = 1 To $aResult[0][0]
    ConsoleWrite($aResult[$i][1] & @CRLF) ; Mail subject
    $aAttachments = _OL_ItemAttachmentGet($oOutlook, $aResult[$i][0])
    For $j = 1 To $aAttachments[0][0]
        ConsoleWrite("  " & $aAttachments[$j][2] & @CRLF) ; Name of atachment
    Next
Next

_OL_Close($oOutlook)
Exit