$sString = "AABABD"

ConsoleWrite("Using StringSplit" & @CRLF)
$aString = StringSplit($sString, "")
For $i = 1 To $aString[0]
    Switch $aString[$i]
        Case "A"
            ConsoleWrite("Character " & $i & " is an A" & @CRLF)
        Case "B"
            ConsoleWrite("Character " & $i & " is an B" & @CRLF)
        Case Else
            ConsoleWrite("Character " & $i & " is neither A nor B" & @CRLF)
    EndSwitch
Next

ConsoleWrite(@CRLF & "Using StringMid" & @CRLF)
For $i = 1 To StringLen($sString)
    Switch StringMid($sString, $i, 1)
        Case "A"
            ConsoleWrite("Character " & $i & " is an A" & @CRLF)
        Case "B"
            ConsoleWrite("Character " & $i & " is an B" & @CRLF)
        Case Else
            ConsoleWrite("Character " & $i & " is neither A nor B" & @CRLF)
    EndSwitch
Next