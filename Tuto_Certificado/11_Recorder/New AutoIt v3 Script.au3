#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.10.2
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

#region --- Au3Recorder generated code Start (v3.3.9.5 KeyboardLayout=0000040C)  ---

#region --- Internal functions Au3Recorder Start ---
Func _Au3RecordSetup()
Opt('WinWaitDelay',100)
Opt('WinDetectHiddenText',1)
Opt('MouseCoordMode',0)
Local $aResult = DllCall('User32.dll', 'int', 'GetKeyboardLayoutNameW', 'wstr', '')
If $aResult[1] <> '0000040C' Then
  MsgBox(64, 'Warning', 'Recording has been done under a different Keyboard layout' & @CRLF & '(0000040C->' & $aResult[1] & ')')
EndIf

EndFunc

Func _WinWaitActivate($title,$text,$timeout=0)
	WinWait($title,$text,$timeout)
	If Not WinActive($title,$text) Then WinActivate($title,$text)
	WinWaitActive($title,$text,$timeout)
EndFunc

_AU3RecordSetup()
#endregion --- Internal functions Au3Recorder End ---


Run('C:\Users\jvela\Desktop\AutoIT\Tuto_Certificado\11_Recorder\Annuaire+~\Annuaire.exe')
_WinWaitActivate("Liste Annuaire","")
local $ROW
local $output="output.csv"
local $handle=FileOpen($output,1)

For $i=0 to 10
MouseClick("left",208,152,1,10)
ClipPut("")

Send("{ALT}e")
Sleep(500)
Send("{DOWN}")
Sleep(500)
Send("{DOWN}")
Sleep(500)
Send("{DOWN}")
Sleep(500)
Send("{DOWN}")
Sleep(500)
Send("{ENTER}")
Sleep(500)

$ROW=ClipGet()

FileWrite($handle,$ROW&@CR)
FileFlush($handle)

Send("{DOWN}")

Next
FileClose($handle)

MsgBox(0,"End","Contacts Gathered")

#endregion --- Au3Recorder generated code End ---