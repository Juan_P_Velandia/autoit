#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------
Local $value = "001-041-0000806"
$value = numero_orden($value)
ConsoleWrite($value)
; Script Start - Add your code below here
Func numero_orden($data)
	$data = StringReplace($data, "-", "")
	Local $data_1 = StringLeft($data, 6)  ; Retrieve 5 characters from the left of the string.
	Local $data_2 = StringRight($data_1, 3)
	Local $data_3 = StringRight($data, 7)

	$aString = StringSplit($data_3, "")
	For $i = 1 To $aString[0]
		If $aString[$i] > 0 Then
			Local $pos = $i
;~ 			ConsoleWrite($pos&@CRLF)
			$data_3 = StringRight($data_3, 8 - $pos)
			ExitLoop
			EndIf
	Next
	$data = $data_2 & $data_3
	Return $data
EndFunc   ;==>numero_orden
