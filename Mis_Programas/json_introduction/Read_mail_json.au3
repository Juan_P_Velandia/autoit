;~ Json File Function
;~ Autor William Aguirre
;~ Testing  Juan Pablo Velandia
#include <MsgBoxConstants.au3>
#include <File.au3>
;~ Ejemplo de utilizar la funci�n
 ;Direccion del archivo que conntiene el body del Email
;~ $carpeta = "C:\Users\jvela\OneDrive\Desktop\Autoit\autoit\Mis_Programas\json_introduction/"
;~ $carpeta_j = "C:\Users\jvela\OneDrive\Desktop\Autoit\autoit\Mis_Programas\json_introduction/"
;~ Nombre del archivo este se debe tener
;~ $nombre_archivo = "BodyMail1.txt"
;~ $jsonfile = "retencion_1.json"
;~ create_json($nombre_archivo,$carpeta,$jsonfile,$carpeta_j)


Func create_json($nombre_archivo,$carpeta,$jsonfile,$carpeta_j)

$archivo = FileOpen($carpeta & $nombre_archivo, 0)     ; Abrir archivo
$completo = ""
;Busqueda de informaci�n dentro del archivo de texto sin importar el orden o el numero de lineas
For $i = 1 To _FileCountLines($archivo)
    $linea = FileReadLine($archivo, $i)
;~ 	retorna la posicion en la linea se�alada en el ciclo for
	Local $BCliente = StringInStr($linea, "Nombre o Raz�n Social:")
	Local $BFEcha = StringInStr($linea, "Fecha Emisi�n:")
	Local $BDir = StringInStr($linea, "Direcci�n:")
	Local $BRuc = StringInStr($linea, "RUC / C�dula de Identidad:")
	Local $BTGRet = StringInStr($linea, "TOTAL GENERAL RETENIDO")
;~ 	si esa posicion es diferente de cero entonces adquiera la linea
	If $BCliente > 0 Then
		$cliente = $linea
	ElseIf $BFEcha >0 Then
		$Fecha = $linea
	ElseIf $BDir  >0 Then
		$Dir = $linea
	ElseIf $BRuc >0 Then
		$ruc = $linea
	ElseIf $BTGRet >0 Then
		While 1
			$i = $i + 1
			$linea = FileReadLine($archivo, $i)
			If $linea <> "" then
				$TGRet = $linea
				ExitLoop
			EndIf
		WEnd
	EndIf
	$completo = $completo & $linea & @CRLF
Next
MsgBox(0, "El archivo completo con FileReadLine", $cliente & @LF & $Fecha & @LF & $Dir & @LF & $ruc & @LF & $TGRet)


;Filtro de informaci�n
$Fecha = StringReplace($Fecha, "Fecha Emisi�n:", "")
$cliente = StringReplace($cliente, "Nombre o Raz�n Social:", "")
$Dir = StringReplace($Dir, "Direcci�n:", "")
$ruc = StringReplace($ruc, "RUC / C�dula de Identidad:", "")


;contrucci�n del archivo tipo JSON
;~ lf hace referencia a que termino la linea y salto de linea
$sBody = '{' & @LF
$sBody = $sBody & '"cliente":' & '"' & $cliente & '",' & @LF & @LF
$sBody = $sBody & '"ruc":' & '"' & $ruc & '",' & @LF & @LF
$sBody = $sBody & '"FecEmi":' & '"' & $Fecha & '",' & @LF & @LF
$sBody = $sBody & '"Dir":' & '"' & $Dir & '",' & @LF & @LF
$sBody = $sBody & '"totalRet":' & '"' & $TGRet & '",' & @LF & @LF
$sBody = $sBody &'}'

$archivo = FileOpen($carpeta_j & ($jsonfile), 2)  ;creacion de archivo
FileWrite($archivo, $sBody  & @CRLF)  ; escritura del cuerpo
FileClose($archivo)  ;cerrar archivo
EndFunc

