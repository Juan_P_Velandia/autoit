#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
MouseMove(100,120,50) ; Coordenadas x,y
AutoItSetOption("MouseCoordMode",2) ; Coordenadas relativas a la Calculadora
Run("calc.exe")
Sleep(2500) ; Espera un poco que cargue.
WinActivate("Calculator")

; Pulsa el bot�n izquierdo del rat�n
; En la posici�n de cada bot�n.

MouseClick("left",36,330) ; 7
MouseClick("left",128,320) ; 8
MouseClick("left",290,428) ; +
MouseClick("left",197,430) ; 3
MouseClick("left",197,414) ; 6
MouseClick("left",270,490) ; =

MouseClick("left",122,384) ; 5
MouseClick("left",44,432) ; 1