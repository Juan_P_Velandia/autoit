#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <Word.au3>
#include <MsgBoxConstants.au3>

; Create application object
Local $oWord = _Word_Create()

Local $oDoc = _Word_DocOpen($oWord, "test.doc", Default, Default, True)

local $Found=_Word_DocFind($oDoc,"AutoIT")

msgbox(0,"Found",@extended)

if $Found=1 then
	msgbox(0,"Success","String Found")
Else
	MsgBox(0,"Error","String not found")
EndIf
_Word_DocSaveAs()
_Word_DocClose($oDoc)
