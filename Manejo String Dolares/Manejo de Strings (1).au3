#include <MsgBoxConstants.au3>

; Retrieve the character position of where the string 'white' first occurs in the sentence.
Local $iPosition = StringInStr("001-041-0007806", "0")
MsgBox($MB_SYSTEMMODAL, "", "The search string 'white' first appears at position: " & $iPosition)