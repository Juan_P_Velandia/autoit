#!/usr/bin/perl
# Mettre #!/usr/bin/perl -d � la place pour debugger pas � pas.
#--------------------------------------------------------- 
# $Id: demo.pl,v 1.6 2011/01/01 14:35:07 belovak Exp $
# 
# $RCSfile: demo.pl,v $
# $Author: belovak $
# 
# $Revision: 1.6 $
# $Date: 2011/01/01 14:35:07 $
# 
# $Locker:  $
# $State: Exp $
#--------------------------------------------------------- 
# $szFileLog: demo.pl,v $
# Revision 1.4  2009/04/12 07:29:15  belovak
# 2008 ->2009
#
# Revision 1.3  2008/11/18 22:45:32  belovak
# *** empty log message ***
#
#--------------------------------------------------------- 

#---------------------------------------
# ANNUAIRE - Useful tool for list.
# Copyright (C) 1997-2011  Xavier DANO and David WYSOCKI.
#
# This tool is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This tool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this library; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
#---------------------------------------

#---------------------------------------
# Plugin Annuaire de d�monstration �crit en script PERL 5.6
# Pr�requis : N�cessite l'installation d'un interpr�teur PERL. 
# Par exemple : installer l'outil ActivePerl depuis http://aspn.activestate.com/ASPN/Perl
#---------------------------------------
# Principe de ce plugin : modifier �ventuellement le fichier In -> Out et 
# �crit des statistiques sur le contenu de la s�lection courante :
# nombre de ligne, de mots et de caract�res de la s�lection.
#---------------------------------------
# Cet exemple recense un peu tous les cas de plugins possibles :
# - demande de donn�es compl�mentaires,
# - mise � jour de la s�lection,
# - affichage d'un compte-rendu d'ex�cution...
#---------------------------------------
# Lancement :  <nomplugin> --In=<MonFichierEntree> [--Out=<MonFichierSortie>] [--Log=<MonFichierLog>] [--Rang=<Num�ro de la colonne courante>]
# Nota : Ces arguments sont renseign�s par le programme Annuaire.
# � partir du menu Tools\Plugins, ou Outils\Extension en fran�ais.
# Dans l'exemple ci-dessous : --In, --Out sont obligatoires.
#---------------------------------------

#---------------------------------------
# AVERTISSEMENT
#---------------------------------------
# Une extension Annuaire peut �tre �crite dans n'importe quel langage. 
# Il faut juste que le programme g�re en ligne de commande :
# (facultatif) --In=<nom du fichier d'origine>    (si le plugin agit sur la s�lection courante).
# (facultatif) --Out=<nom du fichier de r�sultat> (si mise � jour souhait�e sur la liste)
# (facultatif) --Log=<nom du fichier de trace>    (si affichage d'un message � la fin d'ex�cution).
# (facultatif) --Rang=<colonne courante>          (si l'utilisateur veut connaitre la colonne courante pour son traitement.
#---------------------------------------

#---------------------------------------
# Librairies PERL.
#---------------------------------------
use strict;
use warnings;
use Getopt::Long;

#---------------------------------------
# Variables globales
#---------------------------------------
my $FLOG;
my $szFileIn;
my $szFileOut;
my $szFileLog;
my $NumCol;

#---------------------------------------
# Log management Function
#---------------------------------------
sub flog{
	my ($szLog) = @_;

	print $FLOG "$szLog\r\n";
	return;
}


##################################
# DEBUT DU SCRIPT
##################################

#---------------------------------------
#D�finitions
#---------------------------------------
my $iNbLines = 0;
my $iNbWords = 0;
my $iNbCars  = 0;
my %HashFreqCar;

#---------------------------------------
# R�cup�ration des arguments de la ligne de commandes.
#---------------------------------------
GetOptions (
			'In=s' 	=> \$szFileIn,   	# string
			'Out=s' => \$szFileOut,  	# string
			'Log=s' => \$szFileLog,  	# string
			'Rang=n'=> \$NumCol 		# integer
);

print "Demonstration plugin '$0'\r\n";

# Control
die "--In=<Input file name>\n" 		if not defined $szFileIn;
die "--Out=<Result file name>\n" 	if not defined $szFileOut;

#---------------------------------------
# Ask question (if necessary)
#---------------------------------------
#print "Do you want upgrading data [yes/NO] : ";
#my $reponse = <STDIN>;
#chop $reponse;

# bUpdate true si Yes
#my $bUpdate = (lc $reponse eq 'yes');
my $bUpdate = 'no';

#---------------------------------------
# Sortie par d�faut pour l'�criture des logs.
#---------------------------------------
if (defined $szFileLog) {
	# Ecriture dans un fichier dont le nom a �t� pass� en argument.
	open $FLOG, '>', $szFileLog or die "Can't open the file $szFileLog  : $!\n";
}
else {
	# pas d�fini : Sortie standard sur la console.
	$FLOG = \*STDOUT;
}

#---------------------------------------
# Affichage de statistiques
#---------------------------------------
flog "Programme : '$0'.";
flog "--In   = '$szFileIn'.";
flog "--Out  = '$szFileOut'.";
flog "--Rang = '$NumCol'.";

#---------------------------------------
# Ouverture des fichiers d'argument --in=<$szFileIn> et --Out=<$szFileOut>
#---------------------------------------
open my $FILE_IN,  '<', $szFileIn  or die "Can't open the file '$szFileIn' in read mode : $!.\n";
open my $FILE_OUT, '>', $szFileOut or die "Can't open the file '$szFileOut' in write mode : $!.\n";

#---------------------------------------
# Parcours ligne � ligne du fichier contenant la s�lection de l'Annuaire
#---------------------------------------
while(my $Line = <$FILE_IN>) {
	# Calcul du nombre de lignes
	$iNbLines++;

	# Calcul du nombre de mots
	$iNbWords++ while ($Line =~ m/(\w+)/xg);

	# Calcul du nombre de caract�res
	$iNbCars += length($Line);

	# Autre exemple : addition par caract�re
	while ($Line =~ /(\w)/xg) {
		$HashFreqCar{$1}++;
	}

	# uc <=> UpperCase <=> mise en majuscule du r�sultat dans le fichier de sortie si $bUpdate est vrai.
	print $FILE_OUT uc $Line if $bUpdate;
}

#---------------------------------------
# Fermeture des fichiers
#---------------------------------------
close $FILE_OUT or die "Imposible de fermer le fichier de sortie '$szFileOut' : $!.\n";
close $FILE_IN  or die "Imposible de fermer le fichier d'entr�e '$szFileIn'   : $!.\n";

#---------------------------------------
# Statistiques.
#---------------------------------------
flog "Nombre de lignes trait�es \t= $iNbLines.";
flog "Nombre de mots trait�s    \t= $iNbWords.";
flog "Nombre de caract�res trait�s\t= $iNbCars.";
flog "Fr�quence des caract�res \t= " .join(q{}, sort {$HashFreqCar{$b} <=> $HashFreqCar{$a}} keys %HashFreqCar);

flog ($bUpdate) ? 'Mise � jour faite!' : 'Pas de mise � jour faite!';
close $FLOG;
