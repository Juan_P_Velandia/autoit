#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         myName

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here

;~ Function that gathers a certain line
Func Readfromfile($path, $number)
;~ 	always start with file open
	Local $handle = FileOpen($path, 0)
	local $line = FileReadLine($handle,$number)
	MsgBox(0,"Line Read",$line)
EndFunc   ;==>Readfromfile

;~ Readfromfile("My_text_file.txt", 1)
;~ Readfromfile("My_text_file.txt", 2)
;~ Readfromfile("My_text_file.txt", 3)
;~ Readfromfile("My_text_file.txt", 4)

;~ Funcion que me permite escribir nuevo contenido en un archivo
Func writeFunction()
	EndFunc