#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Juan Pablo Velandia

 Script Function:
	Template AutoIt script.

#ce ----------------------------------------------------------------------------

; Script Start - Add your code below here
#include <GUIConstantsEx.au3>

Example()

Func uploadmyfile($path)
	Local $handle = FileOpen($path,0)
	Local $return = ""

	$return = FileRead($handle)
	Return $return
	EndFunc

Func Example()
    ; Create a GUI with various controls.
    Local $hGUI = GUICreate("My_GUI_Example",700,600)
    Local $upload= GUICtrlCreateButton("UPLOAD", 310, 570, 85, 25)
	Local $idOk= GUICtrlCreateButton("EXIT", 190, 570, 85, 25)

;~ 	Too create two taps
	GUICtrlCreateTab(10, 10, 680, 550)

    GUICtrlCreateTabItem("Main")

	GUICtrlCreateLabel("Customized Text",250,50,150,50)
	Local $Imput = GUICtrlCreateInput("Enter the customized text", 20 ,50 , 150 , 50 )

    GUICtrlCreateTabItem("Display")
	Local $display = GUICtrlCreateEdit("",30,30,600,500)


	GUICtrlCreateTabItem("")




    ; Display the GUI.
    GUISetState(@SW_SHOW, $hGUI)

    ; Loop until the user exits.
    While 1
        Switch GUIGetMsg()
            Case $GUI_EVENT_CLOSE, $idOk
                ExitLoop
			Case $upload
				$content = uploadmyfile("File_Lecture.txt")
				GUICtrlSetData($display,$content)
				MsgBox(0, "Upload Ended" ,GUICtrlRead($Imput))

        EndSwitch
    WEnd

    ; Delete the previous GUI and all controls.
    GUIDelete($hGUI)
EndFunc   ;==>Example
